package main;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BA {
	private static final String RETURN_DATE = "07/12/15";
	private static final String DEPART_DATE = "07/10/15";
	private static final String TO = "YUL";
	private static final String FROM = "NYC";
	private static final String REQUEST_BASE = "http://www.ba.com/";
	private static final String PASSWROD = "shortyka1";
	private static final String USER_NAME = "97748611";
//	private static final String USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4";
	public static void main(String[] args) {
		FirefoxProfile profile = new FirefoxProfile();//allProfiles.getProfile("WebDriver");
		
//		profile.setPreference("general.useragent.override", USER_AGENT);

		WebDriver driver = new FirefoxDriver(profile);



		// Go to the Google Suggest home page
		driver.get(REQUEST_BASE);
		WebElement button; 
		try {
			button = driver.findElement(By.name("Continue"));
			button.click();
		} catch (NoSuchElementException e) {}
//		button = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("action-no-app")));
//		button.click();

		WebElement userName = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("loginid")));
		userName.sendKeys(USER_NAME);

		WebElement password = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
		password.sendKeys(PASSWROD);

		button = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("idLoginButton")));
		button.click();
		try {
			driver.navigate().to("https://www.britishairways.com/travel/redeem/execclub/_gf/en_us?eId=106019&tab_selected=redeem&redemption_type=STD_RED");
			WebElement from = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("departurePoint")));
			from.sendKeys(FROM);
			WebElement to = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("destinationPoint")));
			to.sendKeys(TO);
			WebElement departDate = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("departInputDate")));
			departDate.sendKeys(DEPART_DATE);
			WebElement returnDate = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("returnInputDate")));
			returnDate.sendKeys(RETURN_DATE);
			
			button = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("submitBtn")));
			button.click();
			@SuppressWarnings("unused")
			WebElement title = (new WebDriverWait(driver, 25)).until(ExpectedConditions.presenceOfElementLocated(By.id("sector_1")));
			String linkText = "Sorry, there are no flights available on";
			List<WebElement> ret = driver.findElements(By.partialLinkText(linkText));
			ret.addAll(driver.findElements(By.xpath("//*[contains(text(), '"+ linkText +"')]")));
			for (@SuppressWarnings("unused") WebElement webElement : ret) {
				System.out.println("bla");
			}
//			driver.find_elements_by_xpath("//*[contains(text(), 'My Button')]");
			 
			
			String currentUrl = driver.getCurrentUrl();
			// Fix their redirect
			if (currentUrl.startsWith("linkedin://#home")) {
				driver.get(currentUrl.replace("linkedin://#home", "https://touch.www.linkedin.com/"));
				System.out.println("Exception avoided going to " + driver.getCurrentUrl());
			}
			/*WebElement search = */(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"app-header\"]/div/div/div[1]")));
		} catch (Exception e) {
			System.err.println("BAD - we are not sure how to handle this");
		}

//		String currentUrl = driver.getCurrentUrl();
//		currentUrl = currentUrl.subSequence(0, currentUrl.indexOf("&rs=false")) + "&rs=false&or=true&native_launcher=1&memberId=113458434&dl=no#connections/312997182";

//		String cookies = extractCookies(driver.manage().getCookies());
		driver.quit();

	}
}

package main;

import com.google.gson.annotations.SerializedName;

public class Member {

	@SerializedName("authToken") 		public String authToken;
	@SerializedName("distance") 		public int distance;
	@SerializedName("firstName") 		public String firstName;
	@SerializedName("fmt__full_name") 	public String formattedName;
	@SerializedName("headline") 		public String headline;
	@SerializedName("memberID") 		public String id;
	@SerializedName("lastName") 		public String lastName;
	@SerializedName("hasPicture") 		public String hasPicture;
	@SerializedName("mem_pic") 			public boolean picture;
	@SerializedName("tType") 			public String tType;
	
	
	
	
}
/*

"fmt__full_name"
"degree_icon_base__text_plain__degree_icon_symbol"
"link_add_network"
"distance"
"numSharedConnections"
"degree_icon_2__text_plain__NAME_is_2nd_degree_contact_key"
"url_invite_page"section&csrfToken=ajax%3A5064013991033306870&trk=prof-connections-connect",
"mem_pic"
"selfOrConnection"
"pview"
"popup_url"
"degree_icon_2__text_plain__2nd"
"headline"
"memberID"
*/
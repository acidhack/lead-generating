package main;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ResponseObjectMobile {
	@SerializedName("values") 	public List<MemberMobile> values;
	@SerializedName("total") 	public int total; 
	@SerializedName("count") 	public int count;
	@SerializedName("start") 	public int start;
}

package main;

import com.google.gson.annotations.SerializedName;

public class MemberMobile {

	@SerializedName("authToken") 		public String authToken;
	@SerializedName("distance") 		public int distance;
	@SerializedName("firstName") 		public String firstName;
	@SerializedName("formattedName") 	public String formattedName;
	@SerializedName("headline") 		public String headline;
	@SerializedName("id") 				public String id;
	@SerializedName("lastName") 		public String lastName;
	@SerializedName("hasPicture") 		public String hasPicture;
	@SerializedName("picture") 			public boolean picture;
	@SerializedName("tType") 			public String tType;	
}

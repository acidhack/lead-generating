package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.bcel.generic.NEW;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class LinkedIn {
	private static final String REQUEST_BASE = "https://touch.www.linkedin.com/li/v1/people/%s/connections?count=3000";
	private static final String PASSWROD = "ktvt5ynz";
	private static final String USER_NAME = "acidhack@gmail.com";
	private static final String USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";


	public static void main(String[] args) {
		List<String> ids = readPeople();

		if (ids == null || ids.isEmpty()) {
			System.err.println("No people to scan");
			return;
		}
		String cookies = getCookies();

		Date date = new Date();
		
		
		for (String id : ids) {
			ResponseObjectMobile responseObject;
			try {
				responseObject = sendGet(String.format(REQUEST_BASE, id), cookies);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Skipping user : " + id);
				System.err.println(e);
				continue;
			}
			writeData(id, date, responseObject);
		}
	}


	private static String getCookies() {
		FirefoxProfile profile = new FirefoxProfile();//allProfiles.getProfile("WebDriver");
		profile.setPreference("general.useragent.override", USER_AGENT);

		WebDriver driver = new FirefoxDriver(profile);



		// Go to the Google Suggest home page
		driver.get("https://www.linkedin.com/");
		WebElement button; 
		button = driver.findElement(By.id("sign-in-button"));
		button.click();
		button = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("action-no-app")));
		button.click();

		WebElement email = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("session_key-login")));
		email.sendKeys(USER_NAME);

		WebElement password = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("session_password-login")));
		password.sendKeys(PASSWROD);

		button = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id("signin-submit")));
		button.click();
		WebElement search = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"app-header\"]/div/div/div[1]")));

		String currentUrl = driver.getCurrentUrl();
		currentUrl = currentUrl.subSequence(0, currentUrl.indexOf("&or=true")) + "or=true&native_launcher=1&memberId=113458434&dl=no#connections/312997182";

		String cookies = extractCookies(driver.manage().getCookies());
		driver.quit();
		return cookies;
	}


	// HTTP GET request
	private static ResponseObjectMobile sendGet(String url, String cookies) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);


		con.setRequestProperty("cookie", cookies); 

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();


		Type listType = new TypeToken<ResponseObjectMobile>() {}.getType();
		String json = response.toString();
		ResponseObjectMobile responseObject = new Gson().fromJson(json, listType);
		//print result


		return responseObject;
		//		for (Member member : responseObject.values) {
		//			System.out.println((new GsonBuilder()).setPrettyPrinting().create().toJson(member));
		//		}

	}


	private static String extractCookies(Set<Cookie> set) {
		StringBuilder sb = new StringBuilder(); 
		//add the cookies
		for (Cookie cookie : set) {
			sb.append(cookie.getName() + "=" + cookie.getValue() + "; ");
		}
		System.out.println("");
		String string = sb.toString();
		return string;
	}


	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/EMP";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "TestSystemForInvestments!";


	public static List<String> readPeople(){
		List<String> sources = new LinkedList<String>();
		Connection connection = getConnection();
		Statement statement = null;
		if (connection == null) {
			return sources;
		}
		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select linked_in_id from sources;");
			while (rs.next()) {
				sources.add(rs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection!= null) {
					connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sources;

	}

	public static void writeData(String id, Date date, ResponseObjectMobile responseObject) {
		Connection connection = getConnection();
		if (connection == null) {
			return;
		}
		PreparedStatement preparedStatement = null;
		PreparedStatement targetPreparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO connection (auth_token, distance, first_name, formatted_name, headline, connection_id, lastName, hasPicture, picture, tType, user_id, scan_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
			targetPreparedStatement = connection.prepareStatement("INSERT INTO targets (member_id, total, count, scan_date) VALUES (?, ?, ?, ?);");

			targetPreparedStatement.setString(1, id);
			targetPreparedStatement.setInt(2, responseObject.total);
			targetPreparedStatement.setInt(3, responseObject.count);
			targetPreparedStatement.setDate(4, new java.sql.Date(date.getTime()));

			targetPreparedStatement.executeUpdate();

			List<MemberMobile> members = responseObject.values;
			for (MemberMobile member : members) {
				preparedStatement.setString(1, member.authToken);
				preparedStatement.setInt(2, member.distance);
				preparedStatement.setString(3, member.firstName);
				preparedStatement.setString(4, member.formattedName);
				preparedStatement.setString(5, member.headline);
				preparedStatement.setString(6, member.id);
				preparedStatement.setString(7, member.lastName);
				preparedStatement.setString(8, member.hasPicture);
				preparedStatement.setString(9, Boolean.toString(member.picture));
				preparedStatement.setString(10, member.tType);
				preparedStatement.setString(11, id);
				preparedStatement.setDate(12, new java.sql.Date(date.getTime()));

				preparedStatement.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (targetPreparedStatement != null) {
					targetPreparedStatement.close();
				}
				if (connection!= null) {
					connection.close();
				} 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static Connection getConnection(){
		Connection connect = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost/linkedin?"
							+ "user=" + USER + "&password=" + PASS);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connect;

	}

}
